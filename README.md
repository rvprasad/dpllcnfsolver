# DPLLCNFSolver

A tool to solve non-trivial CNF formulae (i.e., with at least one clause) using a DPLL-style algorithm.

The tool provides information about the number of seen/expected variables and seen/expected clauses along with some info about the runtime behavior of the underlying algorithm.  The implementation of the tool in different languages is available in corresponding folder -- _Groovy_ and _Go_.


## Attribution

Copyright (c) 2018, Venkatesh-Prasad Ranganath

Licensed under [BSD 3-clause "New" or "Revised" License](https://choosealicense.com/licenses/bsd-3-clause/)

**Authors:** Venkatesh-Prasad Ranganath
