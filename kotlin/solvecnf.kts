/*
 * Copyright (c) 2019 Venkatesh-Prasad Ranganath
 *
 * BSD 3-clause License
 *
 * Author: Venkatesh-Prasad Ranganath (rvprasad)
 */

@file:CompilerOpts("-jvm-target 1.8")
@file:DependsOn("com.github.ajalt:clikt:2.1.0")
//@file:KotlinOpts("-J-ea")

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import java.io.File
import kotlin.collections.ArrayList
import kotlin.math.abs
import kotlin.math.absoluteValue
import kotlin.text.Regex

private typealias Assignment = Set<Int>
private typealias Clause = List<Int>
private typealias Var = UInt

internal class CommandLine: CliktCommand(name = "kscript solvecnf.kts",
        printHelpOnEmptyArgs = true,
        help = "DPLL based complete CNF solver") {
    private val cnf by option("-c", "--cnf", help = "CNF file in DIMACS format " +
            "(http://www.satcompetition.org/2009/format-benchmarks2009.html)").required()

    override fun run() {
        val (clauses, vars) = readClauses(cnf)
        BasicDPLLCNFSolver.solve(clauses, vars).run {
            if (isEmpty()) {
                println("s UNSATISFIABLE")
            } else {
                println("s SATISFIABLE")
                println("v ${sortedBy(::abs).joinToString(" ")} 0")
            }
        }
    }

    private fun readClauses(fileName: String): Pair<Collection<Clause>, Set<Var>> {
        var expectedNumVars = 0
        var expectedNumClauses = 0
        val infoRegex = Regex("^p cnf \\d+ \\d+.*")
        val clauseRegex = Regex("^\\s*-?\\d.*")
        fun tokenize(line: String) = line.trim().split(' ').filter { it.isNotBlank() }

        val clauses = File(fileName).bufferedReader().lineSequence().map { line ->
            val tokens = tokenize(line)
            when {
                infoRegex.containsMatchIn(line) -> {
                    expectedNumVars = Integer.parseInt(tokens[2])
                    expectedNumClauses = Integer.parseInt(tokens[3])
                    null
                }
                clauseRegex.containsMatchIn(line) -> tokens.map { it.toInt() }.takeWhile { it != 0 }.toMutableList()
                else -> null
            }
        }.filterNotNull().toList()

        val seenVars = clauses.flatten().map { it.absoluteValue.toUInt() }.toSet()
        println("c Variables ${seenVars.size} seen $expectedNumVars expected")
        println("c Clauses ${clauses.size} seen $expectedNumClauses expected")

        return Pair(clauses, seenVars)
    }
}

internal object BasicDPLLCNFSolver {
    private fun simplifyClausesBasedOnValues(clauses: Collection<Clause>, assignment: Assignment) =
            clauses.filterNot { c -> c.any { it in assignment } }.map { c -> c.filterNot { -it in assignment } }

    private sealed class Verdict {
        class Satisfied(val assignment: Assignment): Verdict()
        object Conflicts: Verdict()
        class Unresolved(val clauses: Collection<Clause>, val partialAssignment: Assignment): Verdict()
    }

    private tailrec fun performBCP(clauses: Collection<Clause>, assignment: Assignment = emptySet()): Verdict {
        val valsFromUnitClauses = assignment + clauses.filter { it.size == 1 }.map { it[0] }.toSet()
        val simplifiedClauses = simplifyClausesBasedOnValues(clauses, valsFromUnitClauses)
        val newAssignment = assignment + valsFromUnitClauses
        return when {
            newAssignment.any { -it in newAssignment } || simplifiedClauses.any { it.isEmpty() } -> Verdict.Conflicts
            simplifiedClauses.isEmpty() -> Verdict.Satisfied(newAssignment)
            clauses.size != simplifiedClauses.size -> performBCP(simplifiedClauses, newAssignment)
            else -> Verdict.Unresolved(simplifiedClauses, newAssignment)
        }
    }

    private fun checkAssignment(workItem: BasicWorkItem) = performBCP(workItem.clauses, workItem.assignment)

    fun solve(givenClauses: Collection<Clause>, givenVars: Collection<Var>): Assignment {
        fun prepareAssignment(assignment: Assignment) =
                givenVars.sorted().map { v -> v.toInt().let { if (-it in assignment) -it else it } }.toSet()

        fun solveFromAssignment(clauses: Collection<Clause>, vars: List<Var>, currAssignment: Assignment):
                Assignment {
            fun WorkList.populate(clauses: Collection<Clause>, currAssignment: Assignment, vars: List<Var>) {
                val v1 = vars.first()
                val remainingVars = vars.drop(1)
                add(0, BasicWorkItem(currAssignment + v1.toInt(), remainingVars, clauses))
                add(0, BasicWorkItem(currAssignment + -v1.toInt(), remainingVars, clauses))
            }

            val workList = WorkList().apply { populate(clauses, emptySet(), vars) }
            while (workList.isNotEmpty()) {
                val workItem = workList.removeAt(0)
                when (val ret = checkAssignment(workItem)) {
                    is Verdict.Satisfied -> return prepareAssignment(ret.assignment + currAssignment)
                    is Verdict.Unresolved ->
                        workList.populate(ret.clauses, workItem.assignment + ret.partialAssignment,
                                workItem.remainingVars)
                }
            }

            return emptySet()
        }

        fun removePureLiterals(clauses: Collection<Clause>): Pair<Collection<Clause>, Assignment> {
            val literals = clauses.flatten().distinct()
            val assignment = literals.filterNot { -it in literals }.toSet()
            return Pair(simplifyClausesBasedOnValues(clauses, assignment), assignment)
        }

        val (clauses, assignment) = removePureLiterals(givenClauses)
        return when (val verdict = performBCP(clauses.map { it.distinct() }, assignment)) {
            is Verdict.Conflicts -> emptySet()
            is Verdict.Satisfied -> prepareAssignment(verdict.assignment)
            is Verdict.Unresolved -> {
                val assignment = verdict.partialAssignment
                val vars = givenVars.filterNot { v -> v.toInt().let { it in assignment || -it in assignment } }
                return solveFromAssignment(verdict.clauses, vars, assignment)
            }
        }
    }

    internal data class BasicWorkItem(val assignment: Assignment, val remainingVars: List<Var>,
                                      val clauses: Collection<Clause>)
}

private typealias WorkList = ArrayList<BasicDPLLCNFSolver.BasicWorkItem>

CommandLine().main(args)
